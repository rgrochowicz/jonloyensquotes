# jonloyensquotes.club

## Development

1. Clone this repo
1. `yarn`
1. `yarn start`

## Build
1. `yarn build`

## Deploy
1. Make sure you have correct Google Cloud creds
1. `yarn build`
1. `yarn deploy`
1. Run `Deploy Site` in backing Google Sheet
