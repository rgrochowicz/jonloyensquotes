import React, { Component } from 'react'
import flagImg from './face.png'
import jonImg from './jon.png'

export default class Loader extends Component {
  render () {
    return (
      <div className='loader'>
        <div className='loaderImgContainer'>
          {[0,1,2,3,4].map(e => <img className='loaderBubble' key={e} alt='nerd bubble' src={flagImg} />)}
          <img className='loaderImg' alt='Jon' src={jonImg} />
        </div>
        <div className='loaderText'>~ Loading recent quotes ~</div>
      </div>
    )
  }
}
