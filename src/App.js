import React, { Component } from 'react';
import VelocityTransitionGroup from 'velocity-react/velocity-transition-group'
import createHistory from 'history/createBrowserHistory'
import './App.css';

import Loader from './Loader'

const history = createHistory()

class Container extends Component {
  render () {
    return (
      <div className='app'>
        <App />
        <div className='appBottom'>
          Made with ❤️ using <a href='https://data.world'>data.world</a>
        </div>
      </div>
    )
  }
}

const url = 'https://download.data.world/sql/rgrochowicz/jon-loyens-quotes?query=SELECT%20%2A%20FROM%20%60quotes%60&auth=eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJwcm9kLXVzZXItY2xpZW50OnJncm9jaG93aWN6IiwiaXNzIjoiYWdlbnQ6cmdyb2Nob3dpY3o6OjM4ZTUyN2VjLWJjYjItNDFjZS05ZTFiLTcwNGE1OTczN2U5MSIsImlhdCI6MTQ4OTE4NzkwMywicm9sZSI6WyJ1c2VyX2FwaV93cml0ZSIsInVzZXJfYXBpX3JlYWQiLCJlbXBsb3llZSIsInVzZXIiXSwiZ2VuZXJhbC1wdXJwb3NlIjpmYWxzZSwidXJsIjoiOTczNWZiNTFkNWNjMjAzMmUzZWJmYjY3ZTg3ZmZmOGViYTBjYTA4MSJ9.aPUs3--FHel97wc1jaH2_8hURQuVmLxjtIJSqDCj-p7JAt42TwWbe7q648WtoTzjagzYFyH03WvqoUX6O9JCtg'

const dateFormatter = new Intl.DateTimeFormat(undefined, { timeZone: 'UTC', month: 'short', day: 'numeric', year: 'numeric' })

class App extends Component {
  state = {
    quotes: null,
    selectedQuoteIndex: 0
  }

  constructor (props) {
    super(props)

    const preloadNode = document.getElementById('preload')
    const text = preloadNode.innerText
    if (text && JSON.parse(text)) {
      // eslint-disable-next-line
      this.state.quotes = JSON.parse(text)

      // take from url or set in url
      const currentIndex = this.findIndex(history.location.pathname)
      if (currentIndex > -1) {
        // eslint-disable-next-line
        this.state.selectedQuoteIndex = currentIndex
      } else if (history.location.pathname) {
        setTimeout(() => history.replace('/'))
      }
    }
  }

  componentDidMount () {
    if (!this.state.quotes) {
      this.getQuotes()
    }

    window.addEventListener('keydown', (e) => {
      if (!this.state.quotes) return
      switch (e.keyCode) {
        case 37:
          this.handlePrev(e)
          break
        case 39:
          this.handleNext(e)
          break
        default:
        break
      }
    })

    history.listen((location) => {
      if (!this.state.quotes) return
      const selectedIndex = this.findIndex(location.pathname)
      if (selectedIndex < 0) return

      this.setState({
        selectedQuoteIndex: selectedIndex
      })
    })
  }

  findIndex (pathname) {
    let selectedIndex = -1
    this.state.quotes.forEach((q, i) => {
      if (q.slug === pathname.slice(1)) {
        selectedIndex = i
      }
    })
    return selectedIndex
  }

  async getQuotes () {
    this.setState({
      quotes: null,
      selectedQuoteIndex: 0
    })

    const delayed = new Promise(resolve => {
      setTimeout(resolve, 1500)
    })

    const results = await fetch(url).then(r => r.json())
    const quotes = results.results.bindings
    const transformed = quotes.map(q => {
      return {
        quote: q.v_0.value,
        date: q.v_1.value,
        slug: q.v_2.value
      }
    })

    await delayed

    this.setState({
      quotes: transformed
    })

    // take from url or set in url
    const currentIndex = this.findIndex(history.location.pathname)
    if (currentIndex > -1) {
      this.setState({
        selectedQuoteIndex: currentIndex
      })
    } else if (history.location.pathname) {
      history.replace('/')
    }
  }

  handlePrev = (e) => {
    e.preventDefault()

    const {
      quotes,
      selectedQuoteIndex
    } = this.state

    let ni = selectedQuoteIndex - 1
    if (ni === -1) { ni = quotes.length - 1 }
    history.replace('/' + this.state.quotes[ni].slug)
  }

  handleNext = (e) => {
    e.preventDefault()

    const {
      quotes,
      selectedQuoteIndex
    } = this.state

    let ni = selectedQuoteIndex + 1
    if (ni === quotes.length) { ni = 0 }
    history.replace('/' + this.state.quotes[ni].slug)
  }

  render() {
    const {
      quotes,
      selectedQuoteIndex
    } = this.state

    let body
    if (!quotes) {
      body = <div key={1} className='loaderAnimContainer'>
        <div className='loaderContainer'>
          <Loader />
        </div>
      </div>
    } else {
      const {
        quote,
        date
      } = quotes[selectedQuoteIndex]

      const dateFormatted = dateFormatter.format(Date.parse(date))

      body = (
        <div key={2} className='quoteBox'>
          <div className='quoteText'>
            <div className='quoteBar'>
              <div className='shim' />
            </div>
            <div className='content'>
              “{quote}”
            </div>
          </div>
          <div className='bottom'>
            <div className='bottomLeft'>
              {selectedQuoteIndex + 1} / {quotes.length}
              <a href='#' className='controlLink' onClick={this.handlePrev}>
                Prev
              </a>
              <a href='#' className='controlLink' onClick={this.handleNext}>
                Next
              </a>
            </div>
            <div className='bottomRight'>
              <div className='quoteAttrib'>
                <span>－ <a href="https://www.linkedin.com/in/jonloyens">Jon A. Loyens</a></span>
                <div className='quoteDate'>{dateFormatted}</div>
              </div>
            </div>
          </div>
        </div>
      )
    }

    // return body

    return <VelocityTransitionGroup className='animContainer' runOnMount enter={{animation: "fadeIn"}} leave={{animation: "fadeOut"}}>
      {body}
    </VelocityTransitionGroup>
  }
}

export default Container;
